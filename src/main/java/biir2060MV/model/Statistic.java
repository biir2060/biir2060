package biir2060MV.model;

import java.util.HashMap;
import java.util.Map;

public class Statistic {

	private Map<Domain, Integer> questionsDomains;
	
	public Statistic() {
		questionsDomains = new HashMap<Domain, Integer>();
	}
	
	public void add(Domain key, Integer value){
		questionsDomains.put(key, value);
	}

	public Map<Domain, Integer> getQuestionsDomains() {
		return questionsDomains;
	}

	public void setQuestionsDomain(Map<Domain, Integer> intrebariDomenii) {
		this.questionsDomains = intrebariDomenii;
	}
	
	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		for(Domain domain : questionsDomains.keySet()){
			sb.append(domain.toString() + ": " + questionsDomains.get(domain) + "\n");
		}
		
		return sb.toString();
	}

}
