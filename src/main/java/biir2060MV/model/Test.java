package biir2060MV.model;

import java.util.LinkedList;
import java.util.List;

public class Test  {

	private List<Question> questions;

	public Test() {
		 questions = new LinkedList<Question>();
	}
	
	public List<Question> getQuestions() {
		return questions;
	}
	
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

}
