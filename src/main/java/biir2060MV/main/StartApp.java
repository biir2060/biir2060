package biir2060MV.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

import biir2060MV.exception.*;
import biir2060MV.model.Question;
import biir2060MV.model.Statistic;

import biir2060MV.controller.AppController;
import biir2060MV.repository.IRepository;
import biir2060MV.repository.QuestionsFileRepository;

//functionalitati
//F01.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//F02.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//F03.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

	private static final String file = "E:\\FACULTATE\\Anul3\\Sem2\\vvss\\src\\main\\java\\biir2060MV\\repository\\intrebari.txt";
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

		IRepository repository=new QuestionsFileRepository(file);
		AppController appController = new AppController(repository);
		
		boolean activ = true;
		String optiune = null;
		
		while(activ){
			
			System.out.println("");
			System.out.println("1.Adauga intrebare");
			System.out.println("2.Creeaza test");
			System.out.println("3.Statistica");
			System.out.println("4.Exit");
			System.out.println("");
			
			optiune = console.readLine();
			
			switch(optiune){
			    case "1" :
				    System.out.println("Adauga enunt:");
				    String enunt=console.readLine();
                    System.out.println("Adauga raspuns1:");
                    String raspuns1=console.readLine();
                    System.out.println("Adauga raspuns2:");
                    String raspuns2=console.readLine();
                    System.out.println("Adauga raspuns3:");
                    String raspuns3=console.readLine();
                    System.out.println("Adauga raspuns corect:");
                    String raspunsCorect=console.readLine();
				    System.out.println("Alege domeniu: \n Biologie \n Istorie \n Geografie \n Matematica \n Franceza");
                    String domeniu=console.readLine();
                    try {
                        appController.addQuestion(new Question(enunt, raspuns1, raspuns2, raspuns3, raspunsCorect, domeniu));
                        System.out.println("Intrebarea a fost adaugata");
                    } catch (DuplicateQuestionException e) {
                        System.err.println(e.getMessage());
                    } catch (InputValidationFailedException e) {
                        System.err.println(e.getMessage());
                    }
                    break;
                case "2" :
                    try {
                        appController.createNewTest();
                        System.out.println("Testul a fost creat");
                    } catch (NotAbleToCreateTestException e) {
                        System.err.println(e.getMessage());
                    }
                    break;
                case "3" :
                    try {
                        appController.loadQuestions();
                        Statistic statistica;
                        statistica = appController.getStatistics();
                        System.out.println(statistica);
                    } catch (NotAbleToCreateStatisticsException e) {
                        System.err.println(e.getMessage());
                    } catch (StorageNotFound storageNotFound) {
                        storageNotFound.printStackTrace();
                        System.err.println(storageNotFound.getMessage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "4" :
                    activ = false;
                    break;
                default:
                    break;
			}
		}
		
	}

}
