package biir2060MV.controller;

import java.util.*;

import biir2060MV.exception.StorageNotFound;
import biir2060MV.model.Domain;
import biir2060MV.model.Question;
import biir2060MV.model.Statistic;
import biir2060MV.model.Test;
import biir2060MV.repository.IRepository;
import biir2060MV.exception.DuplicateQuestionException;
import biir2060MV.exception.NotAbleToCreateStatisticsException;
import biir2060MV.exception.NotAbleToCreateTestException;

public class AppController {
	
	private IRepository repository;
	
	public AppController(IRepository repository) {
		this.repository = repository;
	}
	
	public Question addQuestion(Question question) throws DuplicateQuestionException {
		
		this.repository.addQuestion(question);
		return question;
	}
	
	public boolean exists(Question question){
		return this.repository.exists(question);
	}
	
	public Test createNewTest() throws NotAbleToCreateTestException{

		if(repository.getQuestions().size() < 5)
			throw new NotAbleToCreateTestException("Nu exista suficiente intrebari pentru crearea unui test!(5)");

		if(getNumberOfDistinctDomains() < 5)
			throw new NotAbleToCreateTestException("Nu exista suficiente domenii pentru crearea unui test!(5)");

		List<Question> testIntrebari = new LinkedList<Question>();
		List<Domain> domenii = new LinkedList<Domain>();
		Question question=null;
		Test test = new Test();

		while(testIntrebari.size() != 5){
			question = this.pickRandomQuestion();

			if(!testIntrebari.contains(question) && !domenii.contains(question.getDomeniu())) {
				testIntrebari.add(question);
				domenii.add(question.getDomeniu());
			}
		}

		test.setQuestions(testIntrebari);
		return test;
	}
	
	public void loadQuestions() throws Exception {
		repository.loadQuestions();
	}
	
	public Statistic getStatistics() throws NotAbleToCreateStatisticsException{
		
		if(repository.getQuestions().isEmpty())
			throw new NotAbleToCreateStatisticsException("Repository-ul nu contine nicio intrebare!");
		
		Statistic statistica = new Statistic();

		for(Domain domeniu : getDistinctDomains()){
			statistica.add(domeniu, getQuestionsByDomain(domeniu.toString()).size());
		}
		
		return statistica;
	}

	private Question pickRandomQuestion(){
		List<Question> questions=repository.getQuestions();
		Random random = new Random();
		return questions.get(random.nextInt(questions.size()));
	}

	private int getNumberOfDistinctDomains(){
		return getDistinctDomains().size();

	}

	private Set<Domain> getDistinctDomains(){
		List<Question> questions=repository.getQuestions();
		Set<Domain> domains = new TreeSet<Domain>();
		for(Question intrebre : questions)
			domains.add(intrebre.getDomeniu());
		return domains;
	}

	private List<Question> getQuestionsByDomain(String domain){
		List<Question> questions=repository.getQuestions();
		List<Question> intrebariByDomain = new LinkedList<Question>();
		for(Question question : questions){
			if(question.getDomeniu().toString().equals(domain)){
				intrebariByDomain.add(question);
			}
		}

		return intrebariByDomain;
	}

	private int getNumberOfQuestionsByDomain(String domain){
		return getQuestionsByDomain(domain).size();
	}

}
