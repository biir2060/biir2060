package biir2060MV.repository;

import biir2060MV.exception.DuplicateQuestionException;
import biir2060MV.exception.StorageNotFound;
import biir2060MV.model.Question;

import java.util.List;

public interface IRepository
{
    void loadQuestions() throws Exception;
    void addQuestion(Question i) throws DuplicateQuestionException;
    boolean exists(Question i);
    List<Question> getQuestions();
    void setQuestions(List<Question> questions);
}
