package biir2060MV.repository;

import biir2060MV.exception.DuplicateQuestionException;
import biir2060MV.exception.StorageNotFound;
import biir2060MV.model.Domain;
import biir2060MV.model.Question;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MemoryRepository implements IRepository{

    private List<Question> questions;

    public MemoryRepository() {
        try {
            questions=new ArrayList<>();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadQuestions() throws Exception {
        throw new Exception("No implementation");
    }

    @Override
    public void addQuestion(Question i) throws DuplicateQuestionException {
        if(exists(i))
            throw new DuplicateQuestionException("Intrebarea deja exista!");
        questions.add(i);
    }
    @Override
    public boolean exists(Question i){
        for(Question question : questions)
            if(question.equals(i))
                return true;
        return false;
    }
    @Override
    public List<Question> getQuestions() {
        return questions;
    }

    @Override
    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

}
