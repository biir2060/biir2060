package biir2060MV.repository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;


import biir2060MV.exception.StorageNotFound;
import biir2060MV.model.Domain;
import biir2060MV.model.Question;
import biir2060MV.exception.DuplicateQuestionException;

public class QuestionsFileRepository implements IRepository{

    private String filename;
	private List<Question> questions;
	
	public QuestionsFileRepository(String filename) {
		this.filename=filename;
        try {
            this.loadQuestions();
        } catch (StorageNotFound storageNotFound) {
            this.setQuestions(new LinkedList<Question>());
        }
    }

    @Override
    public void loadQuestions() throws StorageNotFound {
        this.setQuestions(loadQuestionsFromFile(filename));
    }

    @Override
    public void addQuestion(Question i) throws DuplicateQuestionException {
		if(exists(i))
			throw new DuplicateQuestionException("Intrebarea deja exista!");
		questions.add(i);
	}
	@Override
	public boolean exists(Question i){
		for(Question question : questions)
			if(question.equals(i))
				return true;
		return false;
	}
    @Override
    public List<Question> getQuestions() {
        return questions;
    }


	private List<Question> loadQuestionsFromFile(String f) throws StorageNotFound {
		
		List<Question> intrebari = new LinkedList<Question>();
		BufferedReader br = null; 
		String line = null;
		List<String> intrebareAux;
		Question question;
		
		try{
			br = new BufferedReader(new FileReader(f));
			line = br.readLine();
			while(line != null){
				intrebareAux = new LinkedList<String>();
				while(!line.equals("##")){
					intrebareAux.add(line);
					line = br.readLine();
				}
				question = new Question();
				question.setEnunt(intrebareAux.get(0));
				question.setVarianta1(intrebareAux.get(1));
				question.setVarianta2(intrebareAux.get(2));
                question.setVarianta3(intrebareAux.get(3));
				question.setVariantaCorecta(intrebareAux.get(4));
				question.setDomeniu(Domain.valueOf(intrebareAux.get(5)));
				intrebari.add(question);
				line = br.readLine();
			}
		
		}
		catch (IOException e) {
			throw new StorageNotFound("Fisierul cu intrebari nu a fost gasit");
        }
		finally{
			try {
				br.close();
			} catch (IOException e) {
                throw new StorageNotFound("Fisierul cu intrebari nu a fost gasit");
			}
		}
		
		return intrebari;
	}

	@Override
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	
}
