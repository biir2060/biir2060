package biir2060MV.exception;

public class StorageNotFound extends Exception {

    private static final long serialVersionUID = 1L;

    public StorageNotFound(String message) {
        super(message);
    }

}
