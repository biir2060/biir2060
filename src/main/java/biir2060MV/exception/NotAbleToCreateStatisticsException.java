package biir2060MV.exception;

public class NotAbleToCreateStatisticsException extends Exception {

	private static final long serialVersionUID = 1L;

	public NotAbleToCreateStatisticsException(String message) {
		super(message);
	}

}
