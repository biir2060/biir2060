package biir2060MV.exception;

public class DuplicateQuestionException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public DuplicateQuestionException(String message) {
		super(message);
	}

}
