package biir2060MV.controller;

import biir2060MV.exception.*;
import biir2060MV.model.Domain;
import biir2060MV.model.Question;
import biir2060MV.model.Statistic;
import biir2060MV.repository.IRepository;
import biir2060MV.repository.MemoryRepository;
import biir2060MV.repository.QuestionsFileRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Map;

public class IncrementalTopDown
{
    private IRepository repository;
    AppController appController;

    @Before
    public void setUp() {
        repository = new MemoryRepository();
        try {
            Question q1 = new Question("Cat face 2+7=?", "1)patru", "2)trei", "3)cinci", "1", "Matematica");
            Question q2 = new Question("Cat face 2+1=?", "1)patru", "2)trei", "3)cinci", "1", "Istorie");
            Question q3 = new Question("Cat face 1+1=?", "1)patru", "2)trei", "3)cinci", "1", "Geografie");
            Question q4 = new Question("Cat face 3+2=?", "1)patru", "2)trei", "3)cinci", "1", "Franceza");
            Question q5 = new Question("Cat face 5+2=?", "1)patru", "2)trei", "3)cinci", "1", "Biologie");
            ArrayList<Question> questions = new ArrayList<>();
            questions.add(q1);
            questions.add(q2);
            questions.add(q3);
            questions.add(q4);
            questions.add(q5);
            repository.setQuestions(questions);
        }
        catch (InputValidationFailedException e) {
            e.printStackTrace();
        }
        appController = new AppController(repository);
    }

    @Test
    public void F01()
    {
        try
        {
            int before=repository.getQuestions().size();
            appController.addQuestion(new Question("Cat face 2+2=?","1)patru","2)trei","3)cinci","1","Matematica"));
            int after=repository.getQuestions().size();
            assert (after-before==1);
        }catch (DuplicateQuestionException e) {
            e.printStackTrace();
            assert(false);
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
            assert (false);
        }
    }

    @Test
    public void F02()
    {
        //sau F02_TC3 pt ca nr de iteratii e random
        try
        {
            biir2060MV.model.Test test=appController.createNewTest();
            if(test==null) {
                assert (false);
            }
            if(!(test.getQuestions().size()==5)) {
                assert (false);
            }
            assert (true);
        } catch (NotAbleToCreateTestException e) {
            e.printStackTrace();
            assert (false);
        }
    }

    @Test
    public void F03()
    {
        try {
            repository.loadQuestions();
            Statistic statistic=appController.getStatistics();
            Map<Domain, Integer> questions=statistic.getQuestionsDomains();
            assert(questions.keySet().size()==5);
            for(Domain domain:questions.keySet()) {
                assert (questions.get(domain).intValue() == 1);
            }
        } catch (NotAbleToCreateStatisticsException e) {
            e.printStackTrace();
            assert(false);
        } catch (StorageNotFound storageNotFound) {
            storageNotFound.printStackTrace();
            assert(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void IntegrareA()
    {
        try
        {
            int before=repository.getQuestions().size();
            appController.addQuestion(new Question("Cat face 2+2=?","1)patru","2)trei","3)cinci","1","Matematica"));
            int after=repository.getQuestions().size();
            assert (after-before==1);
        }catch (DuplicateQuestionException e) {
            e.printStackTrace();
            assert(false);
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
            assert (false);
        }
    }

    @Test
    public void IntegrareAB()
    {
        try {
            repository.loadQuestions();
        } catch (StorageNotFound storageNotFound) {
            storageNotFound.printStackTrace();
            assert (false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        boolean questionAdded = false;
        try {
            int before = repository.getQuestions().size();
            appController.addQuestion(new Question("Cat face 2+2=?", "1)patru", "2)trei", "3)cinci", "1", "Matematica"));
            int after = repository.getQuestions().size();
            assert (after - before == 1);
            questionAdded = true;
        } catch (DuplicateQuestionException e) {
            e.printStackTrace();
            assert (false);
            questionAdded = false;
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
            assert (false);
            questionAdded = false;
        }

        try {
            biir2060MV.model.Test test = appController.createNewTest();
            if (test == null) {
                assert (false);
            }
            if (!(test.getQuestions().size() == 5)) {
                assert (false);
            }
            assert (true);
        } catch (NotAbleToCreateTestException e) {
            e.printStackTrace();
            assert (false);
        }

    }

    @Test
    public void IntegrareABC() {

        try {
            repository.loadQuestions();
        } catch (StorageNotFound storageNotFound) {
            storageNotFound.printStackTrace();
            assert (false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        boolean questionAdded = false;
        try {
            int before = repository.getQuestions().size();
            appController.addQuestion(new Question("Cat face 2+2=?", "1)patru", "2)trei", "3)cinci", "1", "Matematica"));
            int after = repository.getQuestions().size();
            assert (after - before == 1);
            questionAdded = true;
        } catch (DuplicateQuestionException e) {
            e.printStackTrace();
            assert (false);
            questionAdded = false;
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
            assert (false);
            questionAdded = false;
        }

        try {
            biir2060MV.model.Test test = appController.createNewTest();
            if (test == null) {
                assert (false);
            }
            if (!(test.getQuestions().size() == 5)) {
                assert (false);
            }
            assert (true);
        } catch (NotAbleToCreateTestException e) {
            e.printStackTrace();
            assert (false);
        }

        try {

            Statistic statistic=appController.getStatistics();
            Map<Domain, Integer> questions=statistic.getQuestionsDomains();
            assert(questions.keySet().size()==5);
            for(Domain domain:questions.keySet()) {
                if(questionAdded && domain==Domain.Matematica)
                {
                    assert (questions.get(domain).intValue() == 2);
                }
                else
                {
                    assert (questions.get(domain).intValue() == 1);
                }

            }
        } catch (NotAbleToCreateStatisticsException e) {
            e.printStackTrace();
            assert (false);
        }
    }
}
