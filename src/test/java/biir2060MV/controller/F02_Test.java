package biir2060MV.controller;

import biir2060MV.exception.DuplicateQuestionException;
import biir2060MV.exception.InputValidationFailedException;
import biir2060MV.exception.NotAbleToCreateTestException;
import biir2060MV.model.Question;
import biir2060MV.repository.IRepository;
import biir2060MV.repository.MemoryRepository;
import biir2060MV.repository.QuestionsFileRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class F02_Test {
    private IRepository repository;
    private IRepository repository2;
    private IRepository repository3;
    AppController appController;

    @Before
    public void setUp()
    {
        repository=new MemoryRepository();
        repository2=new MemoryRepository();
        repository3=new MemoryRepository();
        try {
            Question q1=new Question("Cat face 2+7=?","1)patru","2)trei","3)cinci","1","Matematica");
            Question q2=new Question("Cat face 2+1=?","1)patru","2)trei","3)cinci","1","Istorie");
            Question q3=new Question("Cat face 1+1=?","1)patru","2)trei","3)cinci","1","Geografie");
            Question q4=new Question("Cat face 3+2=?","1)patru","2)trei","3)cinci","1","Franceza");
            Question q5=new Question("Cat face 5+2=?","1)patru","2)trei","3)cinci","1","Biologie");
            ArrayList<Question> questions=new ArrayList<>();
            questions.add(q1);
            questions.add(q2);
            questions.add(q3);
            questions.add(q4);
            questions.add(q5);
            repository.setQuestions(questions);

            ArrayList<Question> questions2=new ArrayList<>();
            questions2.add(q1);
            questions2.add(q2);
            questions2.add(q3);
            questions2.add(q4);
            repository2.setQuestions(questions2);

            ArrayList<Question> questions3=new ArrayList<>();
            questions3.add(q1);
            questions3.add(q2);
            questions3.add(q3);
            questions3.add(q5);
            Question q6=new Question("Cat face 9+2=?","1)patru","2)trei","3)cinci","1","Biologie");
            questions3.add(q6);
            repository3.setQuestions(questions3);

        } catch (InputValidationFailedException e) {
            e.printStackTrace();
        }

    }
    @Test
    public void F02_TC04()
    {
        //sau F02_TC3 pt ca nr de iteratii e random
        try
        {
            appController = new AppController(repository);
            biir2060MV.model.Test test=appController.createNewTest();
            if(test==null) {
                assert (false);
            }
            if(!(test.getQuestions().size()==5)) {
                assert (false);
            }
            assert (true);
        } catch (NotAbleToCreateTestException e) {
            e.printStackTrace();
            assert (false);
        }
    }

    @Test
    public void F02_TC01()
    {
        try
        {
            appController = new AppController(repository2);
            biir2060MV.model.Test test=null;
            test=appController.createNewTest();
            if(test!=null) {
                assert (false);
            }
        } catch (NotAbleToCreateTestException e) {
            e.printStackTrace();
            assert (true);
        }
    }

    @Test
    public void F02_TC02()
    {
        try
        {
            appController = new AppController(repository3);
            biir2060MV.model.Test test=null;
            test=appController.createNewTest();
            if(test!=null) {
                assert (false);
            }
        } catch (NotAbleToCreateTestException e) {
            e.printStackTrace();
            assert (true);
        }
    }



}