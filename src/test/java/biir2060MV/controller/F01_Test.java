package biir2060MV.controller;

import biir2060MV.exception.DuplicateQuestionException;
import biir2060MV.exception.InputValidationFailedException;
import biir2060MV.model.Question;
import biir2060MV.repository.IRepository;
import biir2060MV.repository.MemoryRepository;
import biir2060MV.repository.QuestionsFileRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class F01_Test {

    private IRepository repository;
    AppController appController;

    @Before
    public void setUp()
    {
        repository=new MemoryRepository();
        try {
            Question q1=new Question("Cat face 2+7=?","1)patru","2)trei","3)cinci","1","Matematica");
            Question q2=new Question("Cat face 2+1=?","1)patru","2)trei","3)cinci","1","Istorie");
            Question q3=new Question("Cat face 1+1=?","1)patru","2)trei","3)cinci","1","Geografie");
            Question q4=new Question("Cat face 3+2=?","1)patru","2)trei","3)cinci","1","Franceza");
            Question q5=new Question("Cat face 5+2=?","1)patru","2)trei","3)cinci","1","Biologie");
            ArrayList<Question> questions=new ArrayList<>();
            questions.add(q1);
            questions.add(q2);
            questions.add(q3);
            questions.add(q4);
            questions.add(q5);
            repository.setQuestions(questions);
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
        }
        appController = new AppController(repository);
    }
    @Test
    public void TC1_ECP()
    {
        try
        {
            int before=repository.getQuestions().size();
            appController.addQuestion(new Question("Cat face 2+2=?","1)patru","2)trei","3)cinci","1","Matematica"));
            int after=repository.getQuestions().size();
            assert (after-before==1);
        }catch (DuplicateQuestionException e) {
            e.printStackTrace();
            assert(false);
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
            assert (false);
        }
    }

    @Test
    public void TC2_ECP()
    {
        try
        {
            appController.addQuestion(new Question("Cat face 2+2=?","1)aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","2)trei","3)cinci","1","Matematica"));
            assert (false);
        }catch (DuplicateQuestionException e) {
            e.printStackTrace();
            assert(true);
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
            assert (true);
        }
    }

    @Test
    public void TC3_ECP()
    {
        try
        {
            appController.addQuestion(new Question("Cat face 2+2=?","3)unu","2)trei","3)cinci","2","Matematica"));
            assert (false);
        }catch (DuplicateQuestionException e) {
            e.printStackTrace();
            assert(true);
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
            assert (true);
        }
    }

    @Test
    public void TC4_ECP()
    {
        try
        {
            appController.addQuestion(new Question("Cat face 2+2=?","","2)trei","3)cinci","1","Matematica"));
            assert (false);
        }catch (DuplicateQuestionException e) {
            e.printStackTrace();
            assert(true);
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
            assert (true);
        }
    }

    @Test
    public void TC1_BVA()
    {
        try
        {
            appController.addQuestion(new Question("Cat face 2+2=?","","2)doi","3)trei","1","Matematica"));
            assert (false);
        }catch (DuplicateQuestionException e) {
            e.printStackTrace();
            assert(true);
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
            assert (true);
        }
    }


    @Test
    public void TC2_BVA()
    {
        try
        {
            appController.addQuestion(new Question("Cat face 2+2=?","1","2)doi","3)trei","1","Matematica"));
            assert (false);
        }catch (DuplicateQuestionException e) {
            e.printStackTrace();
            assert(true);
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
            assert (true);
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
            assert (true);
        }
    }

    @Test
    public void TC3_BVA()
    {
        try
        {
            appController.addQuestion(new Question("Cat face 2+2=?","1)aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","2)doi","3)trei","1","Matematica"));
            assert (true);
        }catch (DuplicateQuestionException e) {
            e.printStackTrace();
            assert(false);
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
            assert (false);
        }
    }

    @Test
    public void TC4_BVA()
    {
        try
        {
            appController.addQuestion(new Question("Cat face 2+2=?","1)aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","2)doi","3)trei","1","Matematica"));
            assert (true);
        }catch (DuplicateQuestionException e) {
            e.printStackTrace();
            assert(false);
        } catch (InputValidationFailedException e) {
            e.printStackTrace();
            assert (false);
        }
    }



}