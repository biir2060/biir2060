package biir2060MV.controller;

import biir2060MV.exception.InputValidationFailedException;
import biir2060MV.exception.NotAbleToCreateStatisticsException;
import biir2060MV.exception.NotAbleToCreateTestException;
import biir2060MV.exception.StorageNotFound;
import biir2060MV.model.Domain;
import biir2060MV.model.Question;
import biir2060MV.model.Statistic;
import biir2060MV.repository.IRepository;
import biir2060MV.repository.MemoryRepository;
import biir2060MV.repository.QuestionsFileRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Map;

public class F03_Test {
    private static final String file = "E:\\FACULTATE\\Anul3\\Sem2\\biir2060\\src\\main\\java\\biir2060MV\\repository\\intrebari.txt";
    private static final String file2 = "E:\\FACULTATE\\Anul3\\Sem2\\biir2060\\src\\main\\java\\biir2060MV\\repository\\intrebariEmpty.txt";

    private IRepository repository;
    private IRepository repositoryEmpty;

    AppController appController;

    @Before
    public void setUp()
    {
        repository=new MemoryRepository();
        repositoryEmpty=new MemoryRepository();
        try {
            Question q1 = new Question("Cat face 2+7=?", "1)patru", "2)trei", "3)cinci", "1", "Matematica");
            Question q2 = new Question("Cat face 2+1=?", "1)patru", "2)trei", "3)cinci", "1", "Istorie");
            Question q3 = new Question("Cat face 1+1=?", "1)patru", "2)trei", "3)cinci", "1", "Geografie");
            Question q4 = new Question("Cat face 3+2=?", "1)patru", "2)trei", "3)cinci", "1", "Franceza");
            Question q5 = new Question("Cat face 5+2=?", "1)patru", "2)trei", "3)cinci", "1", "Biologie");
            ArrayList<Question> questions = new ArrayList<>();
            questions.add(q1);
            questions.add(q2);
            questions.add(q3);
            questions.add(q4);
            questions.add(q5);
            repository.setQuestions(questions);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Test
    public void F03_Valid()
    {
        appController=new AppController(repository);
        try {
            Statistic statistic=appController.getStatistics();
            Map<Domain, Integer> questions=statistic.getQuestionsDomains();
            assert(questions.keySet().size()==5);
            for(Domain domain:questions.keySet()) {
                assert (questions.get(domain).intValue() == 1);
            }
        } catch (NotAbleToCreateStatisticsException e) {
            e.printStackTrace();
            assert(false);
        }
    }

    @Test
    public void F03_Invalid()
    {
        appController=new AppController(repositoryEmpty);
        try {
            appController.getStatistics();
            assert(false);
        } catch (NotAbleToCreateStatisticsException e) {
            e.printStackTrace();
            assert(true);
        }

    }
}